export default {
  START_URL: './csv',
  OUTPUT_URL: './generate-csv',
  OLD_URL: './oldCsv',
  OUTPUT_FILE: `./generate-csv/output.csv`,
}
