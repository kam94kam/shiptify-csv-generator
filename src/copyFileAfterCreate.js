import fs from 'fs'; // Используем обычный модуль fs
import { createObjectCsvWriter as createCsvWriter } from 'csv-writer';
import { HelpersService } from "./helper/index.js";
import GlobalConfig from "./config.js";
import { logger } from "./helper/logger.js";

const {
  copyFile
} = HelpersService;
const {
  START_URL, OUTPUT_FILE, OLD_URL
} = GlobalConfig;
export async function copyFileAfterCreate(params) {
  const {
    pathStartedFile,
    headersCsvFile,
    dataCsvFile,
  } = params
  // Переменная для хранения пути к файлу oldCsv
  const oldCsvPath = `${OLD_URL}/${pathStartedFile}`;
  // Переменная для хранения номера версии файла
  let versionNumber = 1;
  // Генерация нового имени файла с учетом версии
  let newFileName = `old(${versionNumber}).csv`;
  // Генерация полного пути к новому файлу
  let newFilePath = `${OLD_URL}/${newFileName}`;

  // Проверка существования файла с новым именем, если такой файл уже есть, увеличиваем версию
  while (fs.existsSync(newFilePath)) {
      versionNumber++;
      newFileName = `old(${versionNumber}).csv`;
      newFilePath = `${OLD_URL}/${newFileName}`;
  }

  // Переименовываем исходный файл
  fs.renameSync(pathStartedFile, newFilePath);

  // Создаем CSV-файл с обновленными данными
  const csvWriter = createCsvWriter({
      path: OUTPUT_FILE,
      header: headersCsvFile,
      fieldDelimiter: ';',
  });

  await csvWriter.writeRecords(dataCsvFile);

  logger.log(`CSV file was move to ${OUTPUT_FILE}`)

  // Копируем файл в директорию start
  const sourceFilePath = OUTPUT_FILE;
  const destinationFilePath = `${START_URL}/start.csv`;
  await copyFile(sourceFilePath, destinationFilePath);
}
