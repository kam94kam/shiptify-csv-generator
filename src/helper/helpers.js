import fs, { promises as fsPromises } from 'fs'; // Используем обычный модуль fs // Используем fs.promises для асинхронных операций
import csv from 'csv-parser';
import GlobalConfig from "../config.js";
import { logger } from "./logger.js";

const {START_URL} = GlobalConfig
async function copyFile(source, destination) {
  try {
    await fsPromises.copyFile(source, destination);
    logger.log(`New CSV file was move to ${destination}`)
  } catch (err) {
    logger.error(`Ошибка при копировании файла: ${err.message}`, );
  }
}
function getRandomTime() {
  const minHour = 6;
  const maxHour = 18; // 19 - 1 = 18, так как включительно до 19
  const minMinute = 0;
  const maxMinute = 59;

  const randomHour = Math.floor(Math.random() * (maxHour - minHour + 1)) + minHour;
  const randomMinute = Math.floor(Math.random() * (maxMinute - minMinute + 1)) + minMinute;

  const formattedHour = String(randomHour).padStart(2, '0');
  const formattedMinute = String(randomMinute).padStart(2, '0');

  return `${formattedHour}:${formattedMinute}`;
}

function addOneDayToFormattedDate(inputDate) {
  const date = inputDate ? new Date(inputDate) : new Date();
  date.setDate(date.getDate() + 1);

  return `${String(date.getDate()).padStart(2, '0')}.${String(date.getMonth() + 1).padStart(2, '0')}.${date.getFullYear()}`;
}

function incrementId(inputString, number = 1) {
// Ищем цифры в конце строки с помощью регулярного выражения
  const match = inputString.match(/(\d+)$|(_\d+)$/);
  let updatedString;

  if (match) {
    // Если найдены цифры в конце строки, увеличиваем их на 1
    const oldNumber = match[0].replace("_", "");
    const newNumber = parseInt(oldNumber) + number;
    updatedString =  inputString.replace(match[0], `_${newNumber}`);
  } else {
    // Если цифры не найдены, добавляем "__1" в конец строки
    updatedString = inputString + "_1";
  }

  return updatedString;
}

function getRandomValue(array, isRandom = false) {
  // Генерируем случайное число от 0 до 1
  const randomNumber = Math.random();

  // Устанавливаем пороговое значение для возвращения null (например, 0.3 - вероятность 30%)
  const nullProbability = 0.3;

  if (randomNumber < nullProbability && isRandom) {
    // Возвращаем null с вероятностью nullProbability
    return null;
  } else {
    // Возвращаем случайное значение из массива
    const randomIndex = Math.floor(Math.random() * array.length);
    return array[randomIndex];
  }
}

async function getStartedFiles() {
  try {
    const files = await fsPromises.readdir(START_URL);
    console.log(files);
    return files;
  } catch (err) {
    throw err;
  }
}

const countRowsInCsv = (filePath) => {
  return new Promise((resolve, reject) => {
    let rowCount = 0;
    fs.createReadStream(filePath)
      .pipe(csv({ separator: ';' }))
      .on('data', () => {
        rowCount++;
      })
      .on('end', () => {
        resolve(rowCount);
      })
      .on('error', reject);
  });
};

async function getFilesAndRowCount() {
  try {
    const files = await fs.promises.readdir(START_URL);
    return await Promise.all(files.map(async (file) => {
      const filePath = `${START_URL}/${ file }`;
      const rowCount = await countRowsInCsv(filePath);
      return { file, rowCount };
    }));
  } catch (err) {
    throw err;
  }
}

function generateDate() {
  const newDate = new Date().toISOString();

  return {
    date: newDate.split('T')[0],
    dateTime: newDate.replace('T', ' ').slice(0, 19),
  }
}

export default {
  copyFile,
  getRandomTime,
  addOneDayToFormattedDate,
  incrementId,
  getRandomValue,
  getFilesAndRowCount,
  countRowsInCsv,
}
