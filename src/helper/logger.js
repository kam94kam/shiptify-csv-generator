export const logger = {
  log: (text) => {
    console.log('\x1b[32m%s\x1b[0m', text); // Зеленый текст
  },
  warn: (text) => {
    console.log('\x1b[33m%s\x1b[0m', text); // Желтый текст
  },
  error: (text) => {
    console.log('\x1b[31m%s\x1b[0m', text); // Красный текст
  }
};

