import fs from 'fs'; // Используем обычный модуль fs
import { promises as fsPromises } from 'fs'; // Используем fs.promises для асинхронных операций
import csv from 'csv-parser';
import { createObjectCsvWriter as createCsvWriter } from 'csv-writer';
import { HelpersService, MokConfig } from "./src/helper/index.js";
import GlobalConfig from "./src/config.js";
import { copyFileAfterCreate } from "./src/copyFileAfterCreate.js";
import { logger } from "./src/helper/logger.js";
// Функции incrementId и getRandomValue остаются теми же


const {
  copyFile,
  getRandomTime,
  addOneDayToFormattedDate,
  incrementId,
  getRandomValue,
  getFilesAndRowCount, countRowsInCsv,
} = HelpersService;

const {
  ZONE_IDS, DOCK_CUSTOMER_IDS,
} = MokConfig;
const {
  START_URL, OUTPUT_FILE
} = GlobalConfig;

function ErrorException(message) {
  this.message = message;
  this.name = "Исключение, определённое пользователем";
}

function checkLength(arr) {
  if (arr.length === 0) throw new ErrorException('Нет файлов для обработки')
}
async function main() {

  try {
    const pathStartedFiles = await getFilesAndRowCount();

    checkLength(pathStartedFiles)

    const { file, rowCount } = pathStartedFiles[0]
    const pathStartedFile = `${ START_URL }/${ file }`;
    console.log(pathStartedFile);

    let headersCsvFile = [];
    const dataCsvFile = [];
    let count = 1;
    await new Promise((resolve, reject) => {
      fs.createReadStream(pathStartedFile)
        .pipe(csv({ separator: ';' }))
        .on('headers', (headerList) => {
          headersCsvFile = headerList.map(header => ({ id: header, title: header }));
        })
        // Не очень работает rowCount
        // Надо пересмотреть моки относительно 1 зоны подгружать кастомера и ставить путь скорее всего масив объектов с моками
        .on('data', (row) => {
          row[ 'Visit ID' ] = incrementId(row[ 'Visit ID' ], 1);
          row[ 'Order ID' ] = incrementId(row[ 'Order ID' ], 1);
          row[ 'Slot ID' ] = incrementId(row[ 'Slot ID' ], 1);
          row[ 'External Ref' ] = incrementId(row[ 'External Ref' ], 1);
          row[ 'Zone ID' ] = getRandomValue(ZONE_IDS);
          row[ 'Dock Customer Id' ] = getRandomValue(DOCK_CUSTOMER_IDS, true);
          row[ 'Date' ] = addOneDayToFormattedDate();
          row[ 'Hour' ] = getRandomTime();

          count += 1;

          dataCsvFile.push(row);
        })
        .on('end', async () => {
          try {
            const params = {
              pathStartedFile,
              headersCsvFile,
              dataCsvFile,
            }
            await copyFileAfterCreate(params)

            resolve();
          } catch (renameError) {
            reject(renameError);
          }
        })
        .on('error', reject);
    });

    // Создаем CSV-файл с обновленными данными
    const csvWriter = createCsvWriter({
      path: OUTPUT_FILE,
      header: headersCsvFile,
      fieldDelimiter: ';',
    });

    await csvWriter.writeRecords(dataCsvFile);

    logger.log('CSV file was written successfully.')

  } catch (err) {
    logger.error(`Error: ${err.message}`)
  }
}
// TODO: ДОбавить возможность обновления scv не создание а именно обновление

main();
